package com.itheima.eurekaprovider.controller;

import com.itheima.eurekaprovider.service.PortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private PortService portService;
    @RequestMapping(value="/hello",method = RequestMethod.GET)
    public String hello() {
        return this.portService.getPort();
    }
}
